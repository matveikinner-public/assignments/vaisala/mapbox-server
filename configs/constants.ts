export const CONFIG_OPTIONS = {
  SHARED: "SHARED",
  CORS: "CORS",
  LOGGER: "LOGGER",
} as const;
