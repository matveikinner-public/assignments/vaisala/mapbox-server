import { registerAs } from "@nestjs/config";
import { CorsOptions } from "@nestjs/common/interfaces/external/cors-options.interface";
import { CONFIG_OPTIONS } from "..";

export default registerAs(
  CONFIG_OPTIONS.CORS,
  (): CorsOptions => ({
    origin: process.env.CORS_ALLOW_ORIGIN?.split(",") || ["localhost"],
    methods: process.env.CORS_ALLOW_METHODS?.split(",") || ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE", "OPTIONS"],
    // allowedHeaders: ["content-type", "authorization"]
    // exposedHeaders: [],
    // credentials: true,
    // maxAge: 0,
    // preflightContinue: true,
    // optionsSuccessStatus: 200,
  })
);
