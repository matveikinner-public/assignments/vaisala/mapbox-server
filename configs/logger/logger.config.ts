import { registerAs } from "@nestjs/config";
import { Params } from "nestjs-pino";
import { CONFIG_OPTIONS } from "..";

export default registerAs(
  CONFIG_OPTIONS.LOGGER,
  (): Params => ({
    pinoHttp: {
      genReqId: (req, res) => {
        const requestId = crypto.randomUUID().toString();

        req.headers["X-Request-ID"] = requestId;
        res.setHeader("X-Request-ID", requestId);

        return requestId;
      },
      timestamp: true,
      level: process.env.LOG_LEVEL || "debug",
    },
  })
);
