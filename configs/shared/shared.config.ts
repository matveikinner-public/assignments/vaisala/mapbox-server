import { registerAs } from "@nestjs/config";
import SharedConfigOptions from "./shared.types";
import { CONFIG_OPTIONS } from "..";

export default registerAs(
  CONFIG_OPTIONS.SHARED,
  (): SharedConfigOptions => ({
    port: parseInt(process.env.PORT) || 3000,
    name: process.env.NODE_ENV ? `MapBox Server - ${process.env.NODE_ENV.toUpperCase()}` : "MapBox Server",
    version: process.env.VERSION || "1.0.0",
    apiPrefix: process.env.API_PREFIX || "api",
  })
);
