interface SharedConfigOptions {
  port: number;
  name: string;
  version: string;
  apiPrefix: string;
}

export default SharedConfigOptions;
