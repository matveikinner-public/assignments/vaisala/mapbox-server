#!/bin/sh

docker run -d --name mapbox-database \
  -p 5433:5432 \
  -e POSTGRES_PASSWORD=postgres \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_DB=postgres \
  -d postgis/postgis:16-3.4-alpine