import { IsIn, IsOptional } from "class-validator";
import { LocationTempUnitType, locationTempUnits } from "../models/location-temp-unit.model";
import { ApiProperty } from "@nestjs/swagger";

export class LocationQueryDto {
  @ApiProperty({
    description: "The temperature unit",
    required: false,
    enum: locationTempUnits,
  })
  @IsOptional()
  @IsIn(locationTempUnits)
  tempUnit?: LocationTempUnitType;
}
