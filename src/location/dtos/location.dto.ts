import { ApiProperty } from "@nestjs/swagger";
import { LocationTempUnitType } from "../models/location-temp-unit.model";

export class LocationDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  city: string;

  @ApiProperty()
  lat: number;

  @ApiProperty()
  lon: number;

  @ApiProperty()
  temp: number;

  @ApiProperty()
  tempUnit: LocationTempUnitType;

  @ApiProperty()
  createdAt: string;

  @ApiProperty()
  updatedAt: string;
}
