import { Test, TestingModule } from "@nestjs/testing";
import { LocationController } from "./location.controller";
import { LocationService } from "./location.service";
import { LocationQueryDto } from "./dtos/location-query.dto";
import { PrismaService } from "prisma/prisma.service";

class MockPrismaService {
  $queryRawUnsafe = jest.fn();
  $transaction = jest.fn();
}

describe("LocationController", () => {
  let controller: LocationController;
  let locationService: LocationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LocationController],
      providers: [
        LocationService,
        {
          provide: PrismaService,
          useClass: MockPrismaService,
        },
      ],
    }).compile();

    controller = module.get<LocationController>(LocationController);
    locationService = module.get<LocationService>(LocationService);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });

  it("getLocations should call service method with correct query DTO", async () => {
    const locationQueryDto: LocationQueryDto = { tempUnit: "celsius" };
    const serviceSpy = jest.spyOn(locationService, "getLocations");

    await controller.getLocations(locationQueryDto);

    expect(serviceSpy).toHaveBeenCalledWith(locationQueryDto);
  });
});
