import {
  Controller,
  FileTypeValidator,
  Get,
  MaxFileSizeValidator,
  ParseFilePipe,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
  Version,
} from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import { LocationService } from "./location.service";
import { LocationFileValidator } from "./validators/location-file.validator";
import { LocationQueryDto } from "./dtos/location-query.dto";
import { TransformInterceptor } from "@shared/interceptors";
import { ApiBody, ApiConsumes, ApiResponse, ApiTags } from "@nestjs/swagger";
import { FileUploadDto } from "./dtos/file-upload.dto";
import { LocationDto } from "./dtos/location.dto";

@ApiTags("Locations")
@Controller("locations")
@UseInterceptors(TransformInterceptor)
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @ApiResponse({
    status: 201,
    description: "Success in attempt to retrieve records",
    type: TransformInterceptor<[LocationDto]>,
  })
  @Version("1")
  @Get()
  getLocations(@Query() locationQueryDto: LocationQueryDto) {
    return this.locationService.getLocations(locationQueryDto);
  }

  @ApiConsumes("multipart/form-data")
  @ApiBody({
    description: "List of locations",
    type: FileUploadDto,
  })
  @ApiResponse({
    status: 201,
    description: "Success in attempt to create records",
    type: [LocationDto],
  })
  @Version("1")
  @Post("uploads")
  @UseInterceptors(FileInterceptor("file"))
  uploadFile(
    @Query() locationQueryDto: LocationQueryDto,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 }),
          new FileTypeValidator({ fileType: "application/json" }),
          new LocationFileValidator(),
        ],
      })
    )
    file: Express.Multer.File
  ) {
    return this.locationService.uploadFile(locationQueryDto, file);
  }
}
