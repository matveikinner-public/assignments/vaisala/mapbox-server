import { Module } from "@nestjs/common";
import { PrismaService } from "prisma/prisma.service";
import { LocationService } from "./location.service";
import { LocationController } from "./location.controller";

@Module({
  providers: [PrismaService, LocationService],
  controllers: [LocationController],
})
export class LocationModule {}
