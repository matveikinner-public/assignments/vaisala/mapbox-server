import { Test, TestingModule } from "@nestjs/testing";
import { LocationService } from "./location.service";
import { PrismaService } from "prisma/prisma.service";
import { LocationDto } from "./dtos/location.dto";
import { LocationQueryDto } from "./dtos/location-query.dto";

class MockPrismaService {
  $queryRawUnsafe = jest.fn();
  $transaction = jest.fn();
}

describe("LocationService", () => {
  let service: LocationService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LocationService,
        {
          provide: PrismaService,
          useClass: MockPrismaService,
        },
      ],
    }).compile();

    service = module.get<LocationService>(LocationService);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  it("should return locations", async () => {
    const locationQueryDto: LocationQueryDto = { tempUnit: "celsius" };

    const expectedLocations: LocationDto[] = [
      {
        id: "1",
        city: "Helsinki",
        lat: 60.1676,
        lon: 24.9421,
        temp: 7.0,
        tempUnit: "celsius",
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      },
    ];

    (prismaService.$queryRawUnsafe as jest.Mock).mockResolvedValue(expectedLocations);

    const locations = await service.getLocations(locationQueryDto);

    expect(locations).toEqual(expectedLocations);
  });
});
