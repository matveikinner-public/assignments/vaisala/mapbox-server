import { Injectable, Logger } from "@nestjs/common";
import { PrismaService } from "prisma/prisma.service";
import { LocationFileModel } from "./models/location-file.model";
import { LocationQueryDto } from "./dtos/location-query.dto";
import { LocationDto } from "./dtos/location.dto";

@Injectable()
export class LocationService {
  private readonly logger = new Logger(LocationService.name);

  constructor(private readonly prismaService: PrismaService) {}

  async getLocations(locationQueryDto: LocationQueryDto): Promise<LocationDto[]> {
    const { tempUnit = "celsius" } = locationQueryDto;

    let temperatureQuery = "temp,";

    if (tempUnit === "fahrenheit") temperatureQuery = "temp * 9/5 + 32 AS temp,";

    const result = await this.prismaService.$queryRawUnsafe<Omit<LocationDto, "tempUnit">[]>(`
      SELECT id, city, ST_Y(coords) AS lat, ST_X(coords) AS lon, ${temperatureQuery} created_at AS "createdAt", updated_at AS "updatedAt"
      FROM location;
    `);

    return result.map((item) => ({ ...item, tempUnit }));
  }

  async uploadFile(locationQueryDto: LocationQueryDto, file: Express.Multer.File) {
    try {
      const arr = JSON.parse(file.buffer.toString("utf-8")) as LocationFileModel[];

      await this.prismaService.$transaction(async (prisma) => {
        for (const item of arr) {
          const { city, lat, lon, temp } = item;

          const rawQuery = `
            INSERT INTO "location" ("id", "city", "coords", "temp", "updated_at")
            VALUES (gen_random_uuid(), '${city}', ST_SetSRID(ST_MakePoint(${parseFloat(lon)}, ${parseFloat(lat)}), 4326), '${temp}', NOW())
            ON CONFLICT ("city") DO UPDATE
            SET "coords" = ST_SetSRID(ST_MakePoint(${parseFloat(lon)}, ${parseFloat(lat)}), 4326),
                "temp" = '${temp}',
                "updated_at" = NOW();
          `;

          await prisma.$executeRawUnsafe(rawQuery);
        }
      });
    } catch (err) {
      this.logger.error("Error in attempt to save JSON file contents to database", err);
      throw err;
    }

    return this.getLocations(locationQueryDto);
  }
}
