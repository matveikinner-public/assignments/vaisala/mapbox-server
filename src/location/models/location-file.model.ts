import { isValidFloat } from "@utils/index";

export type LocationFileModel = {
  city: string;
  lat: string;
  lon: string;
  temp: string;
};

export const isLocationFileModel = (value: unknown): value is LocationFileModel => {
  if (typeof value !== "object" || value === null) return false;

  const expectedProperties: (keyof LocationFileModel)[] = ["city", "lat", "lon", "temp"];

  return expectedProperties.every(
    (prop) =>
      prop in value &&
      typeof (value as LocationFileModel).city === "string" &&
      typeof (value as LocationFileModel).lat === "string" &&
      isValidFloat((value as LocationFileModel).lat) &&
      typeof (value as LocationFileModel).lon === "string" &&
      isValidFloat((value as LocationFileModel).lon) &&
      typeof (value as LocationFileModel).temp === "string" &&
      isValidFloat((value as LocationFileModel).temp)
  );
};
