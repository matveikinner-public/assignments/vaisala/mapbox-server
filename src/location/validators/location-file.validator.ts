import { FileValidator, Injectable, Logger } from "@nestjs/common";
import { isLocationFileModel } from "../models/location-file.model";

@Injectable()
export class LocationFileValidator extends FileValidator {
  private readonly logger = new Logger(LocationFileValidator.name);
  constructor() {
    super({});
  }

  isValid(file?: Express.Multer.File): boolean {
    if (!file || !file.buffer || file.size === 0) {
      this.logger.warn("Warning in attempt to validate location file. The file has no buffer nor size.");
      return false;
    }

    let result: unknown;

    try {
      result = JSON.parse(file.buffer.toString("utf-8"));
    } catch (error) {
      this.logger.warn("Warning in attempt to validate location file. The file does not parse.");
      return false;
    }

    if (!Array.isArray(result)) {
      this.logger.warn("Warning in attempt to validate location file. The file parse result is not array.");
      return false;
    }

    if (!result.every(isLocationFileModel)) {
      this.logger.warn("Warning in attempt to validate location file. Not all items in a JSON are correct type.");
      return false;
    }

    return true;
  }

  buildErrorMessage(file: Express.Multer.File): string {
    this.logger.warn("Warning in attempt to validate location file.", file);
    return "The uploaded file does not contain valid JSON data with the required structure.";
  }
}
