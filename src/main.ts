import { NestFactory } from "@nestjs/core";
import { ValidationPipe, VersioningType } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { CorsOptions } from "@nestjs/common/interfaces/external/cors-options.interface";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { CONFIG_OPTIONS, SharedConfigOptions } from "@configs/index";
import { Logger } from "nestjs-pino";
import { AppModule } from "./app.module";

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });

  const configService = app.get(ConfigService);

  const { port, name, version, apiPrefix } = configService.get<SharedConfigOptions>(CONFIG_OPTIONS.SHARED);
  const corsOptions = configService.get<CorsOptions>(CONFIG_OPTIONS.CORS);

  app.useLogger(app.get(Logger));
  app.flushLogs();

  app.setGlobalPrefix(apiPrefix);
  app.enableCors(corsOptions);
  app.enableVersioning({ type: VersioningType.URI });

  app.useGlobalPipes(
    new ValidationPipe({
      forbidUnknownValues: true,
      transform: true,
      transformOptions: { enableImplicitConversion: true },
      forbidNonWhitelisted: true,
      whitelist: true,
    })
  );

  const config = new DocumentBuilder().setTitle(name).setVersion(version).build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(`${apiPrefix}/docs`, app, document);

  await app.listen(port);
};

void bootstrap();
