export const isValidFloat = (value: string): boolean => !isNaN(parseFloat(value));
